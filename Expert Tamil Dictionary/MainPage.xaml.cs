﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.ViewManagement;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SQLite;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;
using Windows.ApplicationModel.Search;
using Windows.Data.Xml.Dom;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Popups;
using Windows.UI.ApplicationSettings;
using Windows.UI;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Expert_Tamil_Dictionary
{
    using Newtonsoft.Json;

    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Media.Animation;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Expert_Tamil_Dictionary.Common.LayoutAwarePage
    {

        TileUpdater tileUpdater = TileUpdateManager.CreateTileUpdaterForApplication();

        public String a, tost;
        private DispatcherTimer time = new DispatcherTimer();
        public int count = 0, seco = 0;

        private void timer(int secon)
        {

            try
            {
                time.Tick += ShowToastNotification;
                time.Interval = new TimeSpan(0, 0, secon);
                time.Start();

            }
            catch (Exception)
            {
            }

        }

        public int txt = 0;

        public static MainPage Current;

        private SearchPane _searchPane;

        #region Charm Controller
        private void invokeappbar_Click(object sender, RoutedEventArgs e)
        {
            BottomAppBar.IsOpen = true;
            TopAppBar.IsOpen = true;
            //var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            //localSettings.Values["hide"] = "you";

            // Create a simple setting

            // Read data from a simple setting

            // Object value = localSettings.Values["exampleSetting"];
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            _searchPane.Show();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }
        
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            _searchPane.QuerySubmitted += praba;
            _searchPane.SuggestionsRequested += SearchPane_SuggestionsRequested;
            SettingsPane.GetForCurrentView().CommandsRequested += SettingsPaneCommandsRequest;
            try
            {
                BackgroundAccessStatus status = await BackgroundExecutionManager.RequestAccessAsync();
            }
            catch
            {
            }




        }

        private void SettingsPaneCommandsRequest(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            try
            {

                SettingsCommand comm = new SettingsCommand("help", "Help", (handler) =>
                {
                    Popup popup = BuildSettingsItem(new HelpPage(), 348);
                    popup.IsOpen = true;
                });
                SettingsCommand con = new SettingsCommand("feedback", "FeedBacK", (handler) =>
                {
                    Popup popup = BuildSettingsItem(new FeedbackPage(), 348);
                    popup.IsOpen = true;
                });

                SettingsCommand command = new SettingsCommand("about", "About", (handler) =>
                {
                    Popup popup = BuildSettingsItem(new AboutPage(), 348);
                    popup.IsOpen = true;
                });

                args.Request.ApplicationCommands.Add(comm);
                args.Request.ApplicationCommands.Add(con);

                args.Request.ApplicationCommands.Add(command);


            }
            catch
            { }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SettingsPane.GetForCurrentView().CommandsRequested -= SettingsPaneCommandsRequest;
        }
        #endregion
        #region SettingsPanel
  

        public Popup p;
        private Popup BuildSettingsItem(UserControl u, int w)
        {

            p = new Popup();
            Window.Current.Activated += Current_Activated;
            p.Closed += p_Closed;
            p.IsLightDismissEnabled = true;
            p.ChildTransitions = new TransitionCollection();
            p.ChildTransitions.Add(new PaneThemeTransition()
            {
                Edge = (SettingsPane.Edge == SettingsEdgeLocation.Right) ?
                        EdgeTransitionLocation.Right :
                        EdgeTransitionLocation.Left
            });

            u.Width = w;
            u.Height = Window.Current.Bounds.Height;
            p.Child = u;
            try
            {
                p.SetValue(Canvas.LeftProperty, SettingsPane.Edge == SettingsEdgeLocation.Right ? (Window.Current.Bounds.Width - w) : 0);
                p.SetValue(Canvas.TopProperty, 0);
            }
            catch
            {
            }
            return p;
        }



       public void Current_Activated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
            {
                p.IsOpen = false;
            }
        }
       public  void p_Closed(object sender, object e)
        {
            Window.Current.Activated -= this.Current_Activated;
        }
        #endregion
        #region Share Charm
        private void SetUpAsShareSource()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.ShareTextHandler);
        }

        private void ShareTextHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;




            if (!String.IsNullOrEmpty(richtextbox1.Text))
            {
                request.Data.Properties.Title = "Expert Tamil Dictionary";
                request.Data.Properties.Description = "Share Meanings To Your Friends";
                request.Data.SetText(richtextbox1.Text);
            }
            else
            {
                request.FailWithDisplayText("Start Search For Meaning!!!Then Share!!!");
            }
        }
        #endregion

        //private void Button_Click_1(object sender, RoutedEventArgs e)
        //{
        //    Windows.UI.ViewManagement.ApplicationView.TryUnsnap();

        //}
        #region Search Charm
        private void praba(SearchPane sender, SearchPaneQuerySubmittedEventArgs args)
        {
            var queryText = args.QueryText;


            textbox2.Text = queryText;
            this.search(queryText);
            changeTile_Click(queryText);
        }
        void SearchPane_SuggestionsRequested(SearchPane sender, SearchPaneSuggestionsRequestedEventArgs args)
        {
            var queryText = args.QueryText;

            var request = args.Request;
            //try
            //{

            //    var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");


            //    using (SQLiteConnection db = new SQLiteConnection(dbpath))
            //    {

            String wd = queryText;

            //if (wd.Length > 1)
            //{
            var quer = (from x in jsonList
                        where x.StartsWith(wd)
                        select x).Take(6);
            
            foreach (var cat in quer)
            {

                request.SearchSuggestionCollection.AppendQuerySuggestion(cat);

            }


            //}



            //    db.Dispose();
            //    db.Close();
            //}




            //}


            //catch
            //{

            //}

        }
        #endregion

        //public String af()
        //{
        //    Random ran = new Random();
        //    int r = ran.Next(1, 5000);
        //    String af = "";

        //    try
        //    {
        //        var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");
        //        using (SQLiteConnection db = new SQLiteConnection(dbpath))
        //        {

        //            var query = db.Table<embed>().Where(s => s.id.Equals(r));

        //            foreach (var stock in query)
        //            {
        //                af = stock.word.ToString();
        //            }

        //            db.Dispose();
        //            db.Close();
        //        }
        //    }

        //    catch (Exception)
        //    {

        //    }
        //    return af;
        //}
        //public String aw(String af)
        //{
        //    Random ran = new Random();
        //    int r = ran.Next(1, 5000);
        //    String aw = "";

        //    try
        //    {
        //        var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");
        //        using (SQLiteConnection db = new SQLiteConnection(dbpath))
        //        {

        //            var query = db.Table<embed>().Where(s => s.word.Equals(af));

        //            foreach (var stock in query)
        //            {
        //                aw = stock.meaning.ToString();
        //            }

        //            db.Dispose();
        //            db.Close();
        //        }
        //    }

        //    catch (Exception)
        //    {

        //    }
        //    return aw;
        //}

        #region Tile & Badge
        private void changeTile_Click(String add)
        {


            string ti;
            if (richtextbox1.Text != "")
            {
                ti = "<tile>"
                                 + "<visual>"
                                 + "<binding template='TileWidePeekImageAndText01'>"
                                 + "<text id='1'>" + add + "-" + tost + "</text>"
                                 + "<image id='1' src='ms-appx:///tile.png' alt='Red image'/>"
                                 + "</binding>"
                                 + "<binding template='TileSquarePeekImageAndText04'>"
                                 + "<text id='1'>" + add + "-" + tost + "</text>"
                                 + "<image id='1' src='ms-appx:///thiruvalluvar.png' alt='Gray image'/>"
                                 + "</binding>"
                                 + "</visual>"
                                 + "</tile>";
                Windows.Data.Xml.Dom.XmlDocument tileDOM = new Windows.Data.Xml.Dom.XmlDocument();
                // load the xml string into the DOM, catching any invalid xml characters 
                tileDOM.LoadXml(ti);

                // create a tile notification
                TileNotification tile = new TileNotification(tileDOM);

                // send the notification to the app's application tile

                TileUpdateManager.CreateTileUpdaterForApplication().Update(tile);
                TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(true);

                count++;
                badgescreen(count);
                //   ShowToastNotification();

            }
            else
            {
                ti = "<tile>"
                                 + "<visual>"
                                 + "<binding template='TileWidePeekImageAndText01'>"
                                 + "<text id='1'>Improve Your English Skill</text>"
                                 + "<image id='1' src='ms-appx:///tile.png' alt='Red image'/>"
                                 + "</binding>"
                                 + "<binding template='TileSquarePeekImageAndText04'>"
                                 + "<text id='1'>Improve Your English Skill</text>"
                                 + "<image id='1' src='ms-appx:///thiruvalluvar.png' alt='Gray image'/>"
                                 + "</binding>"
                                 + "</visual>"
                                 + "</tile>";
                Windows.Data.Xml.Dom.XmlDocument tileDOM = new Windows.Data.Xml.Dom.XmlDocument();
                // load the xml string into the DOM, catching any invalid xml characters 
                tileDOM.LoadXml(ti);

                // create a tile notification
                TileNotification tile = new TileNotification(tileDOM);

                // send the notification to the app's application tile
                TileUpdateManager.CreateTileUpdaterForApplication().Update(tile);


            }
            // create a DOM

            //new I Don't Think Its Perfect......
            //ScheduledTileNotification tileNotification = new ScheduledTileNotification(tileXml, DateTimeOffset.UtcNow.AddSeconds(10));
            //TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(true);
            //TileUpdateManager.CreateTileUpdaterForApplication().AddToSchedule(tileNotification);

            ///Method 1
            //    //First, we grab the specific template we want to use.
            //  XmlDocument tileData = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquareBlock);

            //  //Then we grab a reference to the node we want to update.
            //  XmlNodeList textData = tileData.GetElementsByTagName("text");

            //  //Then we set the value of that node.
            //  textData[0].InnerText = "31";
            //  textData[1].InnerText = "Days of Windows 8";

            //  //Then we create a TileNotification object with that data.
            //  TileNotification notification = new TileNotification(tileData);

            //  //We can optionally set an expiration date on the notification.
            //  notification.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(30);

            //  //Finally, we push the update to the tile.
            //  TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);

            ///Method 2

            //private void LargeImageTileUpdateButton_Click(object sender, RoutedEventArgs e)
            //{
            //    //Create the Large Tile exactly the same way.
            //    XmlDocument largeTileData = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWideSmallImageAndText04);
            //    XmlNodeList largeTextData = largeTileData.GetElementsByTagName("text");
            //    XmlNodeList imageData = largeTileData.GetElementsByTagName("image");
            // largeTextData[0].InnerText = "Funny cat";

            //  largeTextData[1].InnerText = "This cat looks like it's trying to eat your face.welcomeby praba";
            //    ((XmlElement)imageData[0]).SetAttribute("src", "ms-appx:///Assets/9-XAML-CatImage.png");

            //    //((XmlElement)imageData[0]).SetAttribute("src", "http://jeffblankenburg.com/downloads/9-XAML-CatImage.png");

            //    //Create a Small Tile notification also (not required, but recommended.)
            //    XmlDocument smallTileData = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquarePeekImageAndText04);
            //    XmlNodeList smallTileText = smallTileData.GetElementsByTagName("text");
            //    XmlNodeList smallTileImage = smallTileData.GetElementsByTagName("image");
            //   // smallTileText[0].InnerText = "Funny cat";
            //    smallTileText[0].InnerText = "This cat looks like it's trying to eat your face.";
            //    ((XmlElement)smallTileImage[0]).SetAttribute("src", "ms-appx:///Assets/9-XAML-CatImageSmall.png");

            //    //Merge the two updates into one <visual> XML node
            //    IXmlNode newNode = largeTileData.ImportNode(smallTileData.GetElementsByTagName("binding").Item(0), true);
            //    largeTileData.GetElementsByTagName("visual").Item(0).AppendChild(newNode);

            //    //Create the notification the same way.
            //    TileNotification notification = new TileNotification(largeTileData);
            //    notification.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(30);

            //    //Push the update to the tile.
            //    TileUpdateManager.CreateTileUpdaterForApplication().Update(notification);

            //}
        }


        private void badgescreen(int cow)
        {
            BackgroundAccessStatus status = BackgroundExecutionManager.GetAccessStatus();

            if ((status == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity) ||
                (status == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity))
            {
                XmlDocument badgeData = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
                XmlNodeList badgeXML = badgeData.GetElementsByTagName("badge");

                badgeData.LoadXml(string.Format("<badge value=\"{0}\" />", cow));

                ((XmlElement)badgeXML[0]).SetAttribute("value", "Playing");

                BadgeNotification badge = new BadgeNotification(badgeData);
                BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badge);

            }
        }
        #endregion

        #region Toast Controller
        private void ShowToastNotification(Object sender, Object e)
        {
            //var xml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);

            //xml.GetElementsByTagName("text")[0].AppendChild(xml.CreateTextNode(a));

            //ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(xml));
            Random ran = new Random();
            int r = ran.Next(1, 61259);

            ToastTemplateType toastType = ToastTemplateType.ToastImageAndText02;
            XmlDocument toastXML = ToastNotificationManager.GetTemplateContent(toastType);
            XmlNodeList toastText = toastXML.GetElementsByTagName("text");
            XmlNodeList toastImages = toastXML.GetElementsByTagName("image");

            try
            {
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");
                using (SQLiteConnection db = new SQLiteConnection(dbpath))
                {

                    var query = db.Table<embed>().Where(s => s.id.Equals(r));

                    foreach (var stock in query)
                    {
                        toastText[0].InnerText = stock.word.ToString();
                        toastText[1].InnerText = stock.meaning.ToString();
                    }

                    db.Dispose();
                    db.Close();
                }
            }

            catch (Exception ex)
            {
                richtextbox1.Text += ex.Message.ToString();
            }


            ((XmlElement)toastImages[0]).SetAttribute("src", "ms-appx:///Assets/Storelogo.png");
            // ((XmlElement)toastImages[0]).SetAttribute("alt", "Scary Cat Face");

            ToastNotification toast = new ToastNotification(toastXML);

            ToastNotificationManager.CreateToastNotifier().Show(toast);

        }
        #endregion

        #region AppOnLoad
        public MainPage()
        {
            this.InitializeComponent();

            Current = this;
            // timer();
            richtextbox1.FontSize = 16;
            listbox2.Visibility = Visibility.Collapsed;
            delselect.Visibility = Visibility.Collapsed;
            delall.Visibility = Visibility.Collapsed;
            createtable();
            _searchPane = SearchPane.GetForCurrentView();

            SetUpAsShareSource();

            locally();
            locally1();

            //RegisterBackgroundTask();
            ProjectFile();


            Window.Current.SizeChanged += (object sender, Windows.UI.Core.WindowSizeChangedEventArgs e) =>
            {
                ApplicationViewState myViewState = ApplicationView.Value;

                if (myViewState == ApplicationViewState.Snapped)
                {


                    //textblock.Height = 103;
                    //textblock.Width = 300;
                    //textblock.Margin = new Thickness(10, 10, 0, 0);

                    //textbox2.Width = 240;
                    //textbox2.Height = 50;
                    //textbox2.Margin = new Thickness(0, 0, 0, 560);

                    //richtextbox1.Height = 405;
                    //richtextbox1.Width = 320;
                    //richtextbox1.Margin = new Thickness(0,205,710,0);

                    //listbox1.Width = 320;
                    //listbox1.Height = 155;
                    //listbox1.Margin = new Thickness(0, 50, 710, 0);

                    //listbox2.Width = 320;
                    //listbox2.Height = 155;
                    //listbox2.Margin = new Thickness(0, 50, 710, 0);




                    //ApplicationViewState.Snapped=true;
                }
                else if (myViewState == ApplicationViewState.Filled)
                {


                    //textblock.Height = 103;
                    //textblock.Width = 644;
                    //textblock.Margin = new Thickness(418, 34, 0, 0);

                    //textbox2.Width = 240;
                    //textbox2.Height = 50;
                    //textbox2.Margin = new Thickness(0, 0, 0, 560);

                    //richtextbox1.Height = 550;
                    //richtextbox1.Width = 703;
                    //richtextbox1.Margin = new Thickness(320, 60, 7, 0);

                    //listbox1.Width = 320;
                    //listbox1.Height = 550;
                    //listbox1.Margin = new Thickness(0, 60, 710, 0);

                    //listbox2.Width = 320;
                    //listbox2.Height = 550;
                    //listbox2.Margin = new Thickness(0, 60, 710, 0);



                }

                else if (myViewState != ApplicationViewState.Filled && myViewState != ApplicationViewState.Snapped)
                {


                    //textblock.Height = 103;
                    //textblock.Width = 644;
                    //textblock.Margin = new Thickness(418, 34, 0, 0);

                    //textbox2.Width = 240;
                    //textbox2.Height = 50;
                    //textbox2.Margin = new Thickness(0, 0, 0, 560);

                    //richtextbox1.Height = 550;
                    //richtextbox1.Width = 703;
                    //richtextbox1.Margin = new Thickness(320, 60, 7, 0);

                    //listbox1.Width = 320;
                    //listbox1.Height = 550;
                    //listbox1.Margin = new Thickness(0, 60, 710, 0);

                    //listbox2.Width = 320;
                    //listbox2.Height = 550;
                    //listbox2.Margin = new Thickness(0, 60, 710, 0);


                }

            };




        }
        // private async void RegisterBackgroundTask()
        //{
        //    try
        //    {
        //        BackgroundAccessStatus status = await BackgroundExecutionManager.RequestAccessAsync();
        //        if (status == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity || status == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity)
        //        {
        //            bool isRegistered = BackgroundTaskRegistration.AllTasks.Any(x => x.Value.Name == "Notification task");
        //            if (!isRegistered)
        //            {
        //                BackgroundTaskBuilder builder = new BackgroundTaskBuilder
        //                {
        //                    Name = "Notification task",
        //                    TaskEntryPoint =
        //                        "BackgroundTask.NotificationTask.NotificationTask"
        //                };
        //                builder.SetTrigger(new TimeTrigger(15, false));

        //                BackgroundTaskRegistration task = builder.Register();
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {

        //    }

        //}


        public string jsonString = null;

        public List<string> jsonList = new List<string>();
        public async void ProjectFile()
        {
            //settings

            try
            {
                var uri = new Uri("ms-appx:///encrypted.db3");
                var file = await StorageFile.GetFileFromApplicationUriAsync(uri);
                var destinationFolder = ApplicationData.Current.LocalFolder;
                // var f = await destinationFolder.GetFileAsync("encrypted.db3");
                await file.CopyAsync(destinationFolder);
            }
            catch { }


            try
            {
                var fil = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///dictionary.txt"));
                await fil.CopyAsync(ApplicationData.Current.LocalFolder);
            }
            catch { }


            try
            {
                StorageFile files = await ApplicationData.Current.LocalFolder.GetFileAsync("dictionary.txt");
                using (StreamReader sr = new StreamReader(await files.OpenStreamForReadAsync()))
                {
                    jsonString = await sr.ReadToEndAsync();
                }
                if (jsonString != null)
                {
                    jsonList = JsonConvert.DeserializeObject<List<string>>(jsonString);
                }
            }
            catch
            {
            }


            //catch (Exception ex)
            //{
            //    if (ex.Message.Equals("Cannot create a file when that file already exists. (Exception from HRESULT: 0x800700B7)"))
            //    {
            //        textbox2.Text = "";
            //    }
            //    else
            //    {
            //        richtextbox1.Text = ex.Message;

            //    }
            //}
        }
        #endregion

        #region Database Controls
        public class embed
        {
            [MaxLength(10), PrimaryKey, AutoIncrement]
            public int id { get; set; }
            [MaxLength(255)]
            public String word { get; set; }
            [MaxLength(255)]
            public String meaning { get; set; }

        }
        public class wol
        {
            public string word { get; set; }
        }
        public class person
        {

            [MaxLength(255), PrimaryKey]
            public String word { get; set; }
            [MaxLength(255)]
            public String meaning { get; set; }
        }

        private void createtable()
        {
            try
            {
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                using (var db = new SQLite.SQLiteConnection(dbpath))
                {
                    // Create the tables if they don't exist
                    db.CreateTable<person>();
                    db.Commit();

                    db.Dispose();
                    db.Close();
                }
            }
            catch
            {

            }
        }



        private void deleteselected(object sender, RoutedEventArgs e)
        {

            try
            {
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                using (var db = new SQLite.SQLiteConnection(dbpath))
                {
                    db.Delete<person>(listbox2.SelectedItem.ToString());
                    int index = listbox2.SelectedIndex;
                    listbox2.Items.RemoveAt(index);

                    //var d = from x in db.Table<person>() select x;
                    //listbox2.Items.Clear();
                    //foreach (var sd in d)
                    //{
                    //    listbox2.Items.Add(sd.word.ToString());
                    //}
                    db.Commit();
                    db.Dispose();
                    db.Close();
                }

            }
            catch
            {
            }


        }

        private async void deleteall(object sender, RoutedEventArgs e)
        {
            MessageDialog dial = new MessageDialog("Delete All Recently Searched Words", "Recently Searched Words");

            dial.Commands.Add(new UICommand("No", null));
            dial.Commands.Add(new UICommand("Yes", new UICommandInvokedHandler(this.deleteallconform)));

            dial.CancelCommandIndex = 1;
            dial.DefaultCommandIndex = 0;

            await dial.ShowAsync();


        }
        public void deleteallconform(IUICommand com)
        {
            try
            {
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                using (var db = new SQLite.SQLiteConnection(dbpath))
                {
                    db.DeleteAll<person>();
                    db.Commit();
                    db.Dispose();
                    db.Close();
                    listbox2.Items.Clear();
                }
            }
            catch
            {
            }
        }
        #endregion


        #region TextBox Event Controller
        private void textbox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            listbox1.Visibility = Visibility.Visible;
            listbox2.Visibility = Visibility.Collapsed;
            delselect.Visibility = Visibility.Collapsed;
            delall.Visibility = Visibility.Collapsed;


            if (txt == 0)
            {
                listbox1.Items.Clear();

                if (textbox2.Text != "")
                {



                    String wd = textbox2.Text.ToLower();

                    var quer = (from x in jsonList where x.StartsWith(wd) select x).Take(13);

                    // int i = 0;
                    foreach (var x in quer)
                    {


                        listbox1.Items.Add(x);


                    }
                    if (quer.Count() == 0)
                    {
                        listbox1.Items.Add("Not Found");
                    }


                }

                else
                {
                    a = "Improve Your Skill!Start Search!!";
                    changeTile_Click(a);
                }
            }
        }
        public void richtextbox1_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void textbox2_KeyDown(object sender, KeyRoutedEventArgs e)
        {

            if (textbox2.Text.Length > 12 && e.Key == Windows.System.VirtualKey.Back)
            {
                txt = 1;
            }
            else
            {
                txt = 0;
            }

            if (e.Key == Windows.System.VirtualKey.Enter)
            {

                searched();
            }
            //if (e.Key == Windows.System.VirtualKey.Up)
            //{

            //}
            //if (e.Key == Windows.System.VirtualKey.Down)
            //{
            //    listbox1_KeyDown(sender, e);
            //}



        }
        #endregion

        #region List Event Controller
        public void searched()
        {
            try
            {

                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");


                using (SQLiteConnection db = new SQLiteConnection(dbpath))
                {
                    db.BeginTransaction();
                    String wd = textbox2.Text;
                    richtextbox1.Text = "";

                    var query = from x in db.Table<embed>()
                                where x.word.Equals(wd)
                                select x;


                    foreach (var x in query)
                    {
                        richtextbox1.Text += x.word + "\n" + x.meaning + "\n";
                        tost = x.meaning;
                        changeTile_Click(wd);
                    }
                 
                    db.Dispose();
                    db.Close();


                }
            }

            catch
            {
                richtextbox1.Text += ""; ;
            }
        }


        public void search(String word)
        {
           
            try
            {
                
                String means = "";
                tost = "";
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "encrypted.db3");
                using (SQLiteConnection db = new SQLiteConnection(dbpath))
                {
                    db.BeginTransaction();

                    richtextbox1.Text = "";

                    var query = db.Table<embed>().Where(s => s.word.Equals(word));

                    foreach (var stock in query)
                    {

                        means = stock.meaning;
                        richtextbox1.Text = stock.word + "\n" + stock.meaning;
                        tost = stock.meaning;
                    }
                  
                    changeTile_Click(word);
                    db.Dispose();
                    db.Close();
                }
                {
                     
                    var local = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                    using (var lo = new SQLite.SQLiteConnection(local))
                    {
                        var check = (from x in lo.Table<person>() where x.word.Equals(word) select x).Count();
                        if (check == 0)
                        {
                            lo.Insert(new person() { word = word, meaning = means, });
                        }
                        lo.Commit();
                        lo.Dispose();
                        lo.Close();
                    }
                }

            }
            catch
            {

            }
        }

        private void listtable(object sender, RoutedEventArgs e)
        {
            listbox1.Visibility = Visibility.Collapsed;
            listbox2.Visibility = Visibility.Visible;

            delselect.Visibility = Visibility.Visible;
            delall.Visibility = Visibility.Visible;

            try
            {
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                using (var db = new SQLite.SQLiteConnection(dbpath))
                {
                    var d = from x in db.Table<person>() select x;
                    listbox2.Items.Clear();
                    foreach (var sd in d)
                    {
                        listbox2.Items.Add(sd.word.ToString());
                    }
                    db.Dispose();
                    db.Close();
                }

            }
            catch
            {
            }

        }

        private void listbox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                string asn = listbox2.SelectedItem.ToString();
                richtextbox1.Text = "";
                var dbpath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "ed.db3");
                using (var db = new SQLite.SQLiteConnection(dbpath))
                {
                    //db.BeginTransaction();

                    var d = db.Table<person>().Where(s => s.word.Equals(asn));


                    foreach (var sd in d)
                    {
                        richtextbox1.Text = sd.word.ToString() + "\n" + sd.meaning.ToString() + "\n";
                    }
                    db.Dispose();
                    db.Close();
                }

            }
            catch
            {
            }
        }

        private void listbox2_KeyDown(object sender, KeyRoutedEventArgs e)
        {

        }

        private void listbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

                    if(listbox1.SelectedItem!=null)
                    search(listbox1.SelectedItem.ToString());

        }

        #endregion

        //private void Button_Click_1(object sender, RoutedEventArgs e)
        //{

        //    DefaultLaunch();
        //}
        //async void DefaultLaunch()
        //{
        //    string uriToLaunch = @"http://about.me/prabakaran.a";

        //    // Create a Uri object from a URI string 
        //    var uri = new Uri(uriToLaunch);
        //    var success = await Windows.System.Launcher.LaunchUriAsync(uri);

        //    if (success)
        //    {
        //        // URI launched
        //    }
        //    else
        //    {
        //        // URI launch failed
        //    }
        //}

        #region ListView Activities
        private void listbox1_KeyDown(object sender, KeyRoutedEventArgs e)
        {

        }
        #endregion

        //private void button4_Click(object sender, RoutedEventArgs e)
        //{
        //    richtextbox1.FontSize = 36;
        //    richtextbox1.Text = "The Goal Of The Software Is To Make The User More Convinient To Search The Tamil Meanings For Selected English Words.";
        //}

        //private void button3_Click(object sender, RoutedEventArgs e)
        //{

        //    //Window.Current.Content = an;

        //}




        //private void Button_Click_2(object sender, RoutedEventArgs e)
        //{
        //    Application.Current.Exit();
        //}

        #region Grid View Work
        private void GridView_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }
        #endregion

        #region Timing Toast
        private void gaming(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt16(game.Text) >= 3 && Convert.ToInt16(game.Text) <= 60)
                {

                    seco = Convert.ToInt32(game.Text.ToString());
                    timer(seco);
                    //start.Content = "Started";
                    //stop.Content = "Stop";


                }

                else
                {
                    game.Text = "3-60";
                }
            }
            catch
            {
                game.Text = "3-60";
            }

        }

        private void game_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void stopgame(object sender, RoutedEventArgs e)
        {
            time.Stop();
            //start.Content = "Start";
            //stop.Content = "Stopped";

        }
        #endregion



        //public class jserialize
        //{
        //    public string word { get; set; }
        //}
        #region load
        private  void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {

                //var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, "encrypted.db3");
                //using (var con = new SQLiteConnection(path))
                //{
                //    con.BeginTransaction();
                //    var f = from x in con.Table<embed>().AsParallel() select x.word;
                //   jsonList.Clear();
                //    foreach (var s in f)
                //    {
                //        jsonList.Add(s);
                //    }

                //    string d = JsonConvert.SerializeObject(jsonList);
                //    var fi = await ApplicationData.Current.LocalFolder.CreateFileAsync("dictionary.txt");
                //    using (StreamWriter sw = new StreamWriter(await fi.OpenStreamForWriteAsync()))
                //    {
                //        await sw.WriteAsync(d);
                //        await sw.FlushAsync();
                //    }
                //}
                //new MessageDialog("complete").ShowAsync();
                //    //SetUpAsShareSource();
                //    //_searchPane.Show();
                SettingsPane.Show();




            }

            catch
            {
            }
        }
        #endregion


        private void gridkeydown(object sender, KeyRoutedEventArgs e)
        {

            //if (e.Key == Windows.System.VirtualKey.Back)
            //{
            //    textbox2.Text+=e.Key.ToString();
            //}

        }

        #region LocalSettings Events
        private void brown(object sender, TappedRoutedEventArgs e)
        {
            //  block1.Foreground = Brushes.Blue;
            //  richtextbox1.Foreground = new SolidColorBrush(Windows.UI.Color.FromArgb(120, 255, 0, 0));



            richtextbox1.Foreground = new SolidColorBrush(Colors.Brown);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "brown";



        }
        private void green(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Green);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "green";
            ///  gridView.Background = new SolidColorBrush(Colors.Blue);

        }
        private void violet(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Violet);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "violet";

        }
        private void yellow(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Yellow);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "yellow";

        }
        private void olive(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Olive);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "olive";

        }
        private void tomato(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Tomato);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "tomato";

        }
        private void lime(object sender, TappedRoutedEventArgs e)
        {
            richtextbox1.Foreground = new SolidColorBrush(Colors.Lime);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;


            localSettings.Values["hide"] = "lime";

        }


        public void locally()
        {
            try
            {
                var localsettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                object a = localsettings.Values["hide"];

                if (a.ToString() == "")
                {
                }
                else if (a.ToString() == "lime")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Lime);
                }
                else if (a.ToString() == "green")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Green);
                }
                else if (a.ToString() == "olive")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Olive);
                }
                else if (a.ToString() == "brown")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Brown);
                }
                else if (a.ToString() == "tomato")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Tomato);
                }
                else if (a.ToString() == "yellow")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Yellow);
                }
                else if (a.ToString() == "violet")
                {
                    richtextbox1.Foreground = new SolidColorBrush(Colors.Violet);

                }

            }
            catch (NullReferenceException)
            {
            }
        }


        public void orange(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.Orange);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "orange";

        }
        public void royalblue(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.RoyalBlue);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "royalblue";

        }
        public void seagreen(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.SeaGreen);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "seagreen";
        }
        public void palevioletred(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.PaleVioletRed);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "palevioletred";
        }
        public void tan(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.Tan);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "tan";

        }
        public void yellowgreen(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.YellowGreen);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "yellowgreen";

        }
        public void hotpink(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.HotPink);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "hotpink";

        }
        public void dogerblue(object sender, TappedRoutedEventArgs e)
        {
            gridView.Background = new SolidColorBrush(Colors.DodgerBlue);
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "dogerblue";

        }
        public void locally1()
        {
            try
            {
                var localsettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                object a = localsettings.Values["gcolor"];

                if (a.ToString() == "")
                {
                }
                else if (a.ToString() == "orange")
                {
                    gridView.Background = new SolidColorBrush(Colors.Orange);
                }
                else if (a.ToString() == "royalblue")
                {
                    gridView.Background = new SolidColorBrush(Colors.RoyalBlue);
                }
                else if (a.ToString() == "seagreen")
                {
                    gridView.Background = new SolidColorBrush(Colors.SeaGreen);
                }
                else if (a.ToString() == "palevioletred")
                {
                    gridView.Background = new SolidColorBrush(Colors.PaleVioletRed);
                }
                else if (a.ToString() == "tan")
                {
                    gridView.Background = new SolidColorBrush(Colors.Tan);
                }
                else if (a.ToString() == "yellowgreen")
                {
                    gridView.Background = new SolidColorBrush(Colors.YellowGreen);
                }
                else if (a.ToString() == "hotpink")
                {
                    gridView.Background = new SolidColorBrush(Colors.HotPink);

                }
                else if (a.ToString() == "dogerblue")
                {
                    gridView.Background = new SolidColorBrush(Colors.DodgerBlue);

                }

            }
            catch (NullReferenceException)
            {
            }
        }
        #endregion
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            var local = Windows.Storage.ApplicationData.Current.LocalSettings;
            local.Values["gcolor"] = "dogerblue";
            gridView.Background = new SolidColorBrush(Colors.DodgerBlue);
            richtextbox1.Foreground = new SolidColorBrush(Colors.Yellow);

            local.Values["hide"] = "yellow";
        }



    }
}

