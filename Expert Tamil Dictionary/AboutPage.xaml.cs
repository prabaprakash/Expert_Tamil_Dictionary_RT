﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;

using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Expert_Tamil_Dictionary
{
    public sealed partial class AboutPage : UserControl
    {
        public AboutPage()
        {
            this.InitializeComponent();
        }

    private void BackButton_Click(object sender, RoutedEventArgs e)
    {
        try
        {
            Popup parent = this.Parent as Popup;
            if (parent != null)
            {
                parent.IsOpen = false;
            }

            // If the app is not snapped, then the back button shows the Settings pane again.
            if (Windows.UI.ViewManagement.ApplicationView.Value != Windows.UI.ViewManagement.ApplicationViewState.Snapped)
            {
                SettingsPane.Show();
            }
        }
        catch
        {
        }
    }

    private async void Button_Click_1(object sender, RoutedEventArgs e)
    {
        string uriToLaunch = @"https://twitter.com/@Prabakaran1993";

        // Create a Uri object from a URI string 
        var uri = new Uri(uriToLaunch);
        var success = await Windows.System.Launcher.LaunchUriAsync(uri);

    }

    private async void Button_Click_2(object sender, RoutedEventArgs e)
    {
        string uriToLaunch = @"http://www.facebook.com/prabakaran1993";

        // Create a Uri object from a URI string 
        var uri = new Uri(uriToLaunch);
        var success = await Windows.System.Launcher.LaunchUriAsync(uri);
    }

    private async void Button_Click_3(object sender, RoutedEventArgs e)
    {
        string uriToLaunch = @"http://www.linkedin.com/pub/praba-prakash/48/35a/6a8";

        // Create a Uri object from a URI string 
        var uri = new Uri(uriToLaunch);
        var success = await Windows.System.Launcher.LaunchUriAsync(uri);
    }

    private async void Button_Click_4(object sender, RoutedEventArgs e)
    {
        string uriToLaunch = @"https://plus.google.com/113387907629278627187";

        // Create a Uri object from a URI string 
        var uri = new Uri(uriToLaunch);
        var success = await Windows.System.Launcher.LaunchUriAsync(uri);
    }

    }
}
